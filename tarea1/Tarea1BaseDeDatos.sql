-- Tabla de Clientes
CREATE TABLE cliente (
    id_cliente SMALLINT,
    id_tienda SMALLINT,
    nombre VARCHAR(30),
    apellido VARCHAR(30),
    correo_electronico VARCHAR(50),
    id_direccion SMALLINT,
    activo BOOLEAN
);

-- Tabla de Actores
CREATE TABLE actor (
    id_actor SMALLINT,
    nombre VARCHAR(30),
    apellido VARCHAR(30),
    ultima_actualizacion DATE
);

-- Tabla de Categorías
CREATE TABLE categoria (
    id_categoria SMALLINT,
    nombre VARCHAR(30), 
    ultima_actualizacion DATE
);

-- Tabla de Películas
CREATE TABLE pelicula (
    id_pelicula SMALLINT,
    titulo VARCHAR(30),
    descripcion VARCHAR(30),
    ano_estreno SMALLINT,
    id_idioma SMALLINT,
    id_idioma_original SMALLINT,
    duracion_alquile VARCHAR(30),
    tarifa_alquiler, DATE
    duracion INTERVAL,
    costo_reemplazo MONEY,
    clasificacion VARCHAR(5), 
    ultima_actualizacion DATE,
    caracteristicas_especiales VARCHAR(50),
    texto_completo VARCHAR(30), 
);

-- Tabla de Actores en Películas
CREATE TABLE actor_pelicula (
    id_actor SMALLINT,
    id_pelicula SMALLINT,
    ultima_actualizacion DATE
);

-- Tabla de Categorías de Películas
CREATE TABLE categoria_pelicula (
    id_pelicula SMALLINT, 
    id_categoria SMALLINT,
    ultima_actualizacion DATE
);

-- Tabla de Direcciones
CREATE TABLE direccion (
    id_direccion SMALLINT,
    direccion VARCHAR(30), 
    direccion2 VARCHAR(30),
    distrito VARCHAR(30), 
    id_ciudad SMALLINT,
    codigo_postal SMALLINT, 
    telefono SMALLINT, 
    ultima_actualizacion DATE
);

-- Tabla de Ciudades
CREATE TABLE ciudad (
    id_ciudad SMALLINT,
    ciudad VARCHAR(30),
    id_pais SMALLINT, 
    ultima_actualizacion DATE
);

-- Tabla de Países
CREATE TABLE pais (
    id_pais SMALLINT,
    pais VARCHAR(30), 
    ultima_actualizacion DATE
);

-- Tabla de Inventario
CREATE TABLE inventario (
    id_inventario SMALLINT,
    id_pelicula SMALLINT,
    id_tienda SMALLINT, 
    ultima_actualizacion DATE
);

-- Tabla de Idiomas
CREATE TABLE idioma (
    id_idioma SMALLINT,
    nombre VARCHAR(30), 
    ultima_actualizacion DATE
);

-- Tabla de Pagos
CREATE TABLE pago (
    id_pago SMALLINT, 
    id_cliente SMALLINT, 
    id_personal SMALLINT,
    id_alquiler SMALLINT,
    monto MONEY,
    fecha_pago DATE 
);

-- Tabla de Rentas
CREATE TABLE renta (
    id_renta SMALLINT,
    fecha_renta DATE,
    id_inventario SMALLINT,
    id_cliente SMALLINT,
    fecha_devolucion DATE,
    id_personal SMALLINT,
    ultima_actualizacion DATE
);

-- Tabla de Personal
CREATE TABLE personal (
    id_personal SMALLINT,
    nombre VARCHAR(30), 
    apellido VARCHAR(30),
    id_direccion SMALLINT, 
    correo_electronico VARCHAR(30),
    id_tienda SMALLINT,
    activo BOOLEAN,
    nombre_usuario VARCHAR(30),
    contrasena VARCHAR(30), 
    ultima_actualizacion DATE 
);

-- Tabla de Tiendas
CREATE TABLE tienda (
    id_tienda SMALLINT,
    id_personal SMALLINT,
    id_direccion SMALLINT, 
    ultima_actualizacion DATE
);
